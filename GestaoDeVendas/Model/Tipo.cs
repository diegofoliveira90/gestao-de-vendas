﻿using System.ComponentModel.DataAnnotations;

namespace GestaoDeVendas.Model
{
    public class Tipo
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
    }
}