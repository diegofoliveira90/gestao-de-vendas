﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GestaoDeVendas.Model
{
    public class Produto
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public Double? valorCompra { get; set; }
        public Double? valorVenda { get; set; }
        public virtual Tipo tipo { get; set; }
    }
}
