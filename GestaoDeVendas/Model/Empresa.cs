﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GestaoDeVendas.Model
{
    public class Empresa
    {
        [Key]
        public int id { get; set; }
        public string nomeFantasia { get; set; }
        public string razaoSocial { get; set; }
        public string cpfCnpj { get; set; }
        public string logradouro { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public string pais { get; set; }

    }
}
