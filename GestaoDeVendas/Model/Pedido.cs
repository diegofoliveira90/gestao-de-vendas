﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GestaoDeVendas.Model
{
    class Pedido
    {
        [Key]
        public int id { get; set; }
        public List<Produto> listaProduto { get; set; }
    }
}
