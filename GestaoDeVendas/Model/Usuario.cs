﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GestaoDeVendas.Model
{
    public class Usuario
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string sobrenome { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }        
    }
}
