﻿using GestaoDeVendas.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace GestaoDeVendas.DAO
{
    class SystemContext:DbContext
    {      

        public DbSet<Usuario> usuario { get; set; }
        public DbSet<Empresa> empresa { get; set; }
        public DbSet<Produto> produto { get; set; }
        public DbSet<Tipo> tipo { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlite("Data Source=blogging.db");
    }
}
